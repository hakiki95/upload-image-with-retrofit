package com.hakiki95.retrofituploadimage.Api;

import com.hakiki95.retrofituploadimage.Model.ResponseApiModel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by hakiki95 on 4/25/2017.
 */

public interface ApiServices {

    @Multipart
    @POST("upload.php")
    Call<ResponseApiModel> uploadImage(@Part MultipartBody.Part ImagePart);

}
