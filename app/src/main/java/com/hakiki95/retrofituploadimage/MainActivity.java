package com.hakiki95.retrofituploadimage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.hakiki95.retrofituploadimage.Api.ApiServices;
import com.hakiki95.retrofituploadimage.Api.RetroClient;
import com.hakiki95.retrofituploadimage.Model.ResponseApiModel;

import java.io.File;
import java.io.IOError;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    Button btnUpload, btnGallery;
    ImageView imgHolder;
    private String part_image;
    private final int REQUEST_GALLERY = 9566;
    private ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnUpload = (Button) findViewById(R.id.btn_upload);
        btnGallery = (Button) findViewById(R.id.btn_gallery);
        imgHolder = (ImageView) findViewById(R.id.imgHolder);
        pd = new ProgressDialog(this);
        pd.setMessage("loading ...");

        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"get image"),REQUEST_GALLERY);
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                File dataImageFile = new File(part_image);
                RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-file"),dataImageFile);
                final MultipartBody.Part partImage = MultipartBody.Part.createFormData("imageupload", dataImageFile.getName(), requestBody);
                ApiServices api = RetroClient.getApiService();
                Call<ResponseApiModel> upload = api.uploadImage(partImage);
                upload.enqueue(new Callback<ResponseApiModel>() {
                    @Override
                    public void onResponse(Call<ResponseApiModel> call, Response<ResponseApiModel> response) {
                        pd.dismiss();
                        Log.d("RETRO", "ONRESPONSE : " + response.body().getPesan());
                        if (response.body().getKode().equals("1"))
                        {
                            Toast.makeText(MainActivity.this, "Image Berhasil di Upload", Toast.LENGTH_SHORT).show();
                            imgHolder.setImageResource(android.R.color.transparent);
                            part_image = null;
                        }else
                        {
                            Toast.makeText(MainActivity.this, "Image Gagal di Upload mak", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseApiModel> call, Throwable t) {
                        Log.d("RETRO", "ON FAILURE ; " + t.getMessage());
                        pd.dismiss();
                    }
                });
            }
        });




    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            if(requestCode == REQUEST_GALLERY)
            {
                Uri dataImage = data.getData();
                String[] dataImageArray = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(dataImage,dataImageArray,null,null,null);

                if (cursor != null)
                {
                    cursor.moveToFirst();
                    int index = cursor.getColumnIndex(dataImageArray[0]);
                    part_image = cursor.getString(index);
                    if(part_image != null)
                    {
                        File imgFile = new File(part_image);
                        imgHolder.setImageBitmap(BitmapFactory.decodeFile(imgFile.getAbsolutePath()));
                    }
                }

            }
        }
    }
}
